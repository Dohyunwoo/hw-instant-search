import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'
import VueRouter from 'vue-router'
import router from '@/router'
import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'
import Styles from '../node_modules/uikit/src/less/uikit.less'
import InstantSearch from 'vue-instantsearch';
import '@/registerServiceWorker'

Vue.config.productionTip = false
Vue.use(VueCompositionApi)
Vue.use(VueRouter)
UIkit.use(Icons) && Styles;
Vue.use(InstantSearch);

new Vue({
    router,
    render: h => <router-view/>
}).$mount('#app')
